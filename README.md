# Pipeline

[![Matrix](https://img.shields.io/badge/Matrix-Join-brightgreen)](https://matrix.to/#/%23pipelineapp:matrix.org)

Pipeline lets you watch and download videos from YouTube and PeerTube, all without needing to navigate through different websites.

## Screenshots
<table>
  <tr>
    <td>
      <img src="/data/screenshots/feed.png" alt="Feed" width="400"/>
    </td>
    <td>
      <img src="/data/screenshots/watch_later.png" alt="Watch later" width="400"/>
    </td>
    <td>
      <img src="/data/screenshots/filters.png" alt="Filters" width="400"/>
    </td>
    <td>
      <img src="/data/screenshots/subscriptions.png" alt="Subscriptions" width="400"/>
    </td>
  </tr>
</table>

## Installation

<table>
  <tr>
    <td>Flatpak</td>
    <td>
      <a href='https://flathub.org/apps/details/de.schmidhuberj.tubefeeder'><img width='130' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-en.png'/></a>
    </td>
  </tr>
  <tr>
    <td>Arch Linux (AUR - stable)</td>
    <td>[tubefeeder](https://aur.archlinux.org/packages/tubefeeder)</td>
  </tr>
  <tr>
    <td>Arch Linux (AUR - git)</td>
    <td>[tubefeeder-git](https://aur.archlinux.org/packages/tubefeeder-git)</td>
  </tr>
</table>

## Features

- Subscribe to channels
- Play videos with any video player
- Filter out unwanted videos in the feed
- Import subscriptions from [NewPipe](https://github.com/TeamNewPipe/NewPipe/) or YouTube
- Multiple platforms
    - YouTube (using Piped as the backend to prevent throttling)
    - PeerTube
    - suggest any other platform with a good API and it will be considered

## Contributing

See the [Contributing page](CONTRIBUTING.md).

## Wiki

Please also take a look at the [wiki](https://gitlab.com/schmiddi-on-mobile/pipeline/-/wikis/home) for more information on how to use the app.

## Donate

If you like this project and have some spare Monero left, consider donating to my Monero address:

Monero:
```
82pRFY4iUjVUWm48ykaTKbjYeDksdMunWPHbrDbTmyKF7PWAxNX8FXM7G6B1n4NFvHfr3ztEg411A2gCjJjNJ8PtEnmcehf
```
